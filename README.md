This repository contains an archive of our CPU-side code and data for comparing the performance of CE-RAM and CPU for FHE. There is code for both B/FV and TFHE, using MS SEAL and PALISADE, respectively.
Contact: jtakeshi@nd.edu
